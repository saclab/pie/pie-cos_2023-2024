# PIE-COS_2023-2024

## LMS page: 

La page générale sur LMS correspondant aux des PIE :  https://lms.isae.fr/course/view.php?id=1356

## This repo

https://gitlab.isae-supaero.fr/-/ide/project/saclab/pie/pie-cos_2023-2024

## Zoom link



Join Zoom Meeting: Forum PIE-COS 2023-2024 - 18 Septembre 13h30 - 17h
https://zoom.us/j/96166700128?pwd=ZXJnaFVhR1pZY1JSYzNkdHFBeVFYZz09

 
Pour les personnes venant sur site, la sécurité a été prévenue, vous pouvez nous rejoindre directement en Amphi 2.

(ticket #227475)




